﻿namespace _2048_console
{
    internal class Point
    {
        public Point(uint _x,uint _y)
        {
            x = _x;
            y = _y;
        }
        public uint x { get; set; }
        public uint y { get; set; }
    }
}