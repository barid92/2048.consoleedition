﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2048_console
{
    public static class Render
    {
        public static void WyswietlTablice(int?[,] Tablica, int Size, ConsoleKeyInfo cki)
        {
            Console.Clear();
            for (int i = 0; i < Size; i++)
            {
                for (int j = 0; j < Size; j++)
                {
                    if (Tablica[i, j] == null)
                    {
                        Console.ForegroundColor = ConsoleColor.Gray;
                        Console.Write("-");
                        Console.Write("   ");
                    }
                    else
                    {
                        int? value = Tablica[i, j];
                        string text = Tablica[i, j].ToString();
                        int textCount = text.Count();
                        Console.ForegroundColor = Color(value);
                        Console.Write(text);
                        switch (textCount)
                        {
                            case 1:
                                Console.Write("   ");
                                break;
                            case 2:
                                Console.Write("  ");
                                break;
                            case 3:
                                Console.Write(" ");
                                break;
                            case 4:
                                Console.Write("");
                                break;

                            default:
                                Console.Write("   ");
                                break;
                        }
                        
                    }

                }
                Console.ForegroundColor = ConsoleColor.Gray;
                Console.WriteLine();
            }
            Console.WriteLine(); Console.WriteLine(); Console.WriteLine();
            Console.WriteLine(cki.Key.ToString());
        }
        private static ConsoleColor Color(int? value)
        {
            switch (value)
            {
                case 2:
                    return ConsoleColor.White;
                case 4:
                    return ConsoleColor.Gray;
                case 8:
                    return ConsoleColor.Cyan;
                case 16:
                    return ConsoleColor.DarkCyan;
                case 32:
                    return ConsoleColor.Red;
                case 64:
                    return ConsoleColor.DarkRed;
                case 128:
                    return ConsoleColor.Yellow;
                case 256:
                    return ConsoleColor.Yellow;
                case 512:
                    return ConsoleColor.Yellow;
                case 1028:
                    return ConsoleColor.Yellow;
                case 2048:
                    return ConsoleColor.DarkYellow;

                default:
                    return ConsoleColor.White;
                    break;
            }
        }
    }
}
