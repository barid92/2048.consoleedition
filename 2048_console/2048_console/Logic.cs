﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2048_console
{
    public static class Logic
    {
        public static void MoveDown(int?[,] Tablica, int Size)
        {

            for (int i = 0; i < Size; i++)
            {
                for (int j = Size - 1; j >= 0; j--)
                {
                    bool oneStep = true;
                    for (int k = j + 1; k < Size; k++)
                    {

                        if (oneStep && Tablica[j, i] != null & Tablica[j, i] == Tablica[k, i])
                        {
                            Tablica[k, i] = Tablica[j, i] + Tablica[j, i];
                            Tablica[j, i] = null;
                            oneStep = false;
                        }
                        if (Tablica[k, i] == null && Tablica[j, i] != null)
                        {

                            Tablica[k, i] = Tablica[j, i];
                            Tablica[j, i] = null;
                            j++;

                        }
                    }

                }
            }
        }

        internal static bool IsTheSame(int?[,] tablica, int?[,] tablicaPrzedRuchem, int Size)
        {
            for (int i = 0; i < Size; i++)
            {
                for (int j = 0; j < Size; j++)
                {
                    if (tablica[i, j] != tablicaPrzedRuchem[i, j])
                    {
                        return false;
                    }
                    
                }
            }
            return true;
        }

        internal static int?[,] CloneTablica(int?[,] tablica, int Size)
        {
            var Tablica = new int?[Size, Size];
            for (int i = 0; i < Size; i++)
            {
                for (int j = 0; j < Size; j++)
                {
                    Tablica[i, j] = tablica[i, j];
                }
            }
            return Tablica;
        }

        public static void MoveLeft(int?[,] Tablica, int Size)
        {

            for (int i = 0; i < Size; i++)
            {
                for (int j = 0; j < Size; j++)
                {
                    bool oneStep = true;
                    for (int k = j - 1; k >= 0; k--)
                    {

                        if (Tablica[i, j] == Tablica[i, k] && Tablica[i, j] != null && oneStep)
                        {
                            Tablica[i, k] = Tablica[i, j] + Tablica[i, j];
                            Tablica[i, j] = null;
                            oneStep = false;
                        }
                        if (Tablica[i, k] == null && Tablica[i, j] != null)
                        {
                            Tablica[i, k] = Tablica[i, j];
                            Tablica[i, j] = null; j--;

                        }
                        else
                        {
                            break;
                        }
                    }

                }
            }
        }

        public static void MoveUp(int?[,] Tablica, int Size)
        {
            for (int i = 0; i < Size; i++)
            {
                for (int j = 0; j < Size; j++)
                {
                    bool oneStep = true;
                    for (int k = j - 1; k >= 0; k--)
                    {

                        if (oneStep && Tablica[j, i] != null & Tablica[j, i] == Tablica[k, i])
                        {
                            Tablica[k, i] = Tablica[j, i] + Tablica[j, i];
                            Tablica[j, i] = null;
                            oneStep = false;
                        }
                        if (Tablica[k, i] == null && Tablica[j, i] != null)
                        {

                            Tablica[k, i] = Tablica[j, i];
                            Tablica[j, i] = null;
                            j--;

                        }
                        else
                        {
                            break;
                        }
                    }

                }
            }
        }

        public static void MoveRight(int?[,] Tablica, int Size)
        {
            for (int i = 0; i < Size; i++)
            {
                for (int j = Size - 1; j >= 0; j--)
                {
                    bool oneStep = true;
                    for (int k = j + 1; k < Size; k++)
                    {

                        if (oneStep && Tablica[i, j] != null && Tablica[i, j] == Tablica[i, k])
                        {
                            Tablica[i, k] = Tablica[i, j] + Tablica[i, j];
                            Tablica[i, j] = null;
                            oneStep = false;
                        }
                        if (Tablica[i, k] == null && Tablica[i, j] != null)
                        {

                            Tablica[i, k] = Tablica[i, j];
                            Tablica[i, j] = null;
                            j++;
                        }
                    }

                }
            }
        }
    }
}
