﻿using System;

namespace _2048_console
{
    internal class Game
    {
        private bool IsGameOver = false;
        private ConsoleKeyInfo cki;
        private int?[,] Tablica;
        private const int SIZE = 4;
        internal void init()
        {
            Tablica = new int?[SIZE, SIZE];
        }

        internal void start()
        {
            AddTwo();
            while (!IsGameOver)
            {
                Render.WyswietlTablice(Tablica, SIZE, cki);
                cki = Console.ReadKey();

                NowyRuch();

            }

        }
        private void NowyRuch()
        {
            int?[,] TablicaPrzedRuchem = Logic.CloneTablica(Tablica,SIZE);
            if (cki.Key == ConsoleKey.RightArrow)
            {
                Logic.MoveRight(Tablica,SIZE);
                if (!Logic.IsTheSame(Tablica, TablicaPrzedRuchem,SIZE))
                {
                    AddTwo();
                }
            }
            if (cki.Key == ConsoleKey.LeftArrow)
            {
                Logic.MoveLeft(Tablica, SIZE);
                if (!Logic.IsTheSame(Tablica, TablicaPrzedRuchem, SIZE))
                {
                    AddTwo();
                }
            }
            if (cki.Key == ConsoleKey.UpArrow)
            {
                Logic.MoveUp(Tablica, SIZE);
                if (!Logic.IsTheSame(Tablica, TablicaPrzedRuchem, SIZE))
                {
                    AddTwo();
                }
            }
            if (cki.Key == ConsoleKey.DownArrow)
            {
                Logic.MoveDown(Tablica, SIZE);
                if (!Logic.IsTheSame(Tablica, TablicaPrzedRuchem, SIZE))
                {
                    AddTwo();
                }
            }


        }

        private void AddTwo()
        {
            Point nowyPunkt;
            do
            {
                nowyPunkt = LosujPunkt();
            } while (Tablica[nowyPunkt.x, nowyPunkt.y] != null);
            Tablica[nowyPunkt.x, nowyPunkt.y] = 2;
        }


        private Point LosujPunkt()
        {
            if (IsNull())
            {
                Random kostka = new Random();
                return new Point(Convert.ToUInt32(kostka.Next(SIZE)), Convert.ToUInt32(kostka.Next(SIZE)));
            }
            else
            {
                //Tu trzeba bedzie cos poprawic
                return null;
            }
        }

        private bool IsNull()
        {
            for (int i = 0; i < SIZE; i++)
            {
                for (int j = 0; j < SIZE; j++)
                {
                    if (Tablica[i, j] == null)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}